package HomeWork3;

public class Exercise7 {
    public static void main(String[] args) {
        String str="ab5c2d4ef12s";
        int sum=0;
        for (int i = 0; i < str.length(); i++) {
            char c=str.charAt(i);
            if(Character.isDigit(c)){
                sum+=Character.getNumericValue(c);
            }
        }
        System.out.println(sum);
    }
}
