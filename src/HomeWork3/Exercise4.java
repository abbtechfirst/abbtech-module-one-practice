package HomeWork3;

import java.util.*;

public class Exercise4 {
    public static void main(String[] args) {
        String word=" w3resource";
        Set<Character> chars = new LinkedHashSet<>();
        for (int i = 0; i < word.length(); i++) {
            chars.add(word.charAt(i));
        }
        StringBuilder sb = new StringBuilder();
        for (char c : chars) {
            sb.append(c);
        }
        System.out.println(sb);
    }

}
