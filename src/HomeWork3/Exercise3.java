package HomeWork3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Exercise3 {
    public static void main(String[] args) {
        ArrayList<Character> chars=new ArrayList();
        String word="successes";
        int counter=1;
        int max=0;
        int max2=0;
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < word.length(); i++) {
            chars.add(word.charAt(i));
        }
        for (char c : chars) {
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        System.out.println(map);
        for(int value:map.values()){
            if(value>max){
                max=value;
            }
        }
        for(int value:map.values()){
            if(value>max2){
                if(value<max){
                    max2=value;
                }
            }
        }
        System.out.println(max2);
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue().equals(max2)) {
                System.out.println(entry.getKey());
            }
        }
    }
}
