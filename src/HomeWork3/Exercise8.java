package HomeWork3;

import java.util.Arrays;
import java.util.Comparator;

public class Exercise8 {
    public static void main(String[] args) {
        String[] colors = {"Green", "White", "Black", "Pink",
                "Orange", "Blue", "Champagne", "Indigo", "Ivory"};
        String[] sortedAscending = colors.clone();
        Arrays.sort(sortedAscending, Comparator.comparingInt(String::length));
        System.out.println("Sorted color (ascending order):" + Arrays.toString(sortedAscending));

        String[] sortedDescending = colors.clone();
        Arrays.sort(sortedDescending, (a, b) -> Integer.compare(b.length(), a.length()));
        System.out.println("Sorted color (descending order):" + Arrays.toString(sortedDescending));
}}
