package HomeWork3;

public class Exercise6 {
    public static void main(String[] args) {
        String sentence="Reverse words in a given string";
        System.out.println("The given string is: " + sentence);
        String reversedString = reverseWords(sentence);
        System.out.println("The new string after reversing the words: " + reversedString);
    }

    public static String reverseWords(String str) {
        String[] words = str.split("\\s+");
        StringBuilder reversedString = new StringBuilder();

        for (int i = words.length - 1; i >= 0; i--) {
            reversedString.append(words[i]).append(" ");
        }

        return reversedString.toString().trim();
    }
}
