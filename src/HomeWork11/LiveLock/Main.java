package HomeWork11.LiveLock;

public class Main {
    static Object resource1 = new Object();
    static Object resource2 = new Object();
    public static void main(String[] args) {

        Thread threadA = new Thread(() -> {
            synchronized (resource1) {
                System.out.println("Thread A acquired resource 1");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (resource2) {
                    System.out.println("Thread A needs resource 2 ");
                }
            }
        });

        Thread threadB = new Thread(() -> {
            synchronized (resource2) {
                System.out.println("Thread B acquired resource 2");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (resource1) {
                    System.out.println("Thread B needs resource 1 ");
                }
            }
        });
        threadA.start();
        threadB.start();
    }
}
