package HomeWork11.Starvation;

public class Main {
    static Object lock = new Object();
    public static void main(String[] args) throws InterruptedException {

        Thread threadA = new Thread(() -> {
            synchronized (lock) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        threadA.setPriority(Thread.MAX_PRIORITY);
        threadA.start();
        Thread threadB = new Thread(() -> {
            synchronized (lock) {
                System.out.println("Thread B trying to acquire lock");
            }
        });
        threadB.start();
    }
}
