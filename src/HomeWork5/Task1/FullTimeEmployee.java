package HomeWork5.Task1;

public class FullTimeEmployee implements Employee{
    private int id;
    private String name;
    private double monthlySalary;

    public FullTimeEmployee(int id, String name, double monthlySalary) {
        this.id = id;
        this.name = name;
        this.monthlySalary = monthlySalary;
    }

    @Override
    public double calculateSalary() {
        return monthlySalary;
    }

    @Override
    public void displayDetails() {
        System.out.println("id=" + id +
                ", name='" + name + '\'' +
                ", monthlySalary=" + monthlySalary +
                '}');
    }

}
