package HomeWork5.Task1;

public class PartTimeEmployee implements Employee{
    private int id;
    private String name;
    private double hourlyRate;
    private double hoursWorked;

    public PartTimeEmployee(int id, String name, double hourlyRate, double hoursWorked) {
        this.id = id;
        this.name = name;
        this.hourlyRate = hourlyRate;
        this.hoursWorked = hoursWorked;
    }

    @Override
    public double calculateSalary() {
        return hourlyRate*hoursWorked;
    }

    @Override
    public void displayDetails() {
        System.out.println("id=" + id +
                ", name='" + name + '\'' +
                ", hourlyRate=" + hourlyRate +
                ", hoursWorked=" + hoursWorked +
                '}');
    }


}
