package HomeWork5.Task1;

import java.util.ArrayList;

public class EmployeeTest {
    public static void main(String[] args) {
        Employee emp1=new FullTimeEmployee(1,"aysel",2000);
        Employee emp2=new PartTimeEmployee(2,"aynur",100,20);
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(emp1);
        employees.add(emp2);
        for(Employee emp:employees){
            emp.displayDetails();
        }
    }
}
