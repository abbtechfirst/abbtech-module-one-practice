package HomeWork5.Task2;

public class Outer {
    private static class Inner{
        private static int count=0;
        static int getCount(){
            return count;
        }

        public Inner() {
            count+=1;
        }
    }

    public static void main(String[] args) {
        Inner i=new Outer.Inner();
        Inner i2=new Outer.Inner();
        System.out.println(Inner.getCount());
    }
}
