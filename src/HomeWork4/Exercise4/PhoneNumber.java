package HomeWork4.Exercise4;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumber {
    private String phoneNumber;

    public PhoneNumber(String phoneNumber) throws IllegalArgumentException {
        setPhoneNumber(phoneNumber);
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) throws IllegalArgumentException {
        String regex = "\\+994\\s(50|51|55)\\s\\d{3}-\\d{2}-\\d{2}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phoneNumber);
        if(matcher.matches()){
            this.phoneNumber = phoneNumber;
        }
         else {
            throw new IllegalArgumentException("Invalid phone number format");
        }
    }
}
