package HomeWork4.Exercise5;

public class DayOfWeekTest {
    public static void main(String[] args) {
        for(DayOfWeek day:DayOfWeek.values()){
            System.out.println(day+" "+day.isWeekend());
        }
    }
}
