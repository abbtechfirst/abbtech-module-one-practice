package HomeWork4.Exercise5;

public enum DayOfWeek {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY;

    DayOfWeek() {
    }
    boolean isWeekend(){
        if(this.equals(SUNDAY) || this.equals(SATURDAY)){
            return true;
        }
        return false;
    }


}
