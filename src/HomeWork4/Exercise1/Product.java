package HomeWork4.Exercise1;

public class Product {
    static String companyName;
    static{
        companyName="MyCompany";
    }
    String productName;
    int productID;
    static int newID=1000;
    {
        productID=newID++;
    }

    public Product(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productName='" + productName + '\'' +
                ", productID=" + productID +
                " companyName=" +companyName+
                '}';
    }
}
