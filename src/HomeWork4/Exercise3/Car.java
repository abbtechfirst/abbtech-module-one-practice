package HomeWork4.Exercise3;

public class Car {
    String brand;
    String model;
    int productionYear;
    String fuel;
    Double speed=0.0;

    public Car(String brand, String model, int productionYear, String fuel, Double speed) {
        this.brand = brand;
        this.model = model;
        this.productionYear = productionYear;
        this.fuel = fuel;
        this.speed = speed;
    }
    void speed(double increase){
        this.speed=this.speed+increase;

    }
    void slow(double decrease){
        if(decrease>this.speed){
            speed=0.0;
        }
        else{
            this.speed=this.speed-decrease;}

    }
    double stop(){
        this.speed=0.0;
        return this.speed;
    }
    void checkSpeed(){
        if (this.speed>100){
            System.out.println("warning you are so high");
        }
    }

}
