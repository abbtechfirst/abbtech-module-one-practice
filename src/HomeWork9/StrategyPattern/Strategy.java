package HomeWork9.StrategyPattern;

public interface Strategy {
    public int doOperation(int a, int b);
}
