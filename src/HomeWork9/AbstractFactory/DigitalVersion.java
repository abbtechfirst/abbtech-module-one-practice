package HomeWork9.AbstractFactory;

public class DigitalVersion extends AbstractFactory {
    @Override
    Publication getCategory(String category) {
        if (category.equalsIgnoreCase("book")){
            return new DigitalBook();
        }

        else if (category.equalsIgnoreCase("journal")){
            return new DigitalJournal();
        }
        else if (category.equalsIgnoreCase("journal")){
            return new DigitalEncyclopedia();
        }
        return null;
    }
}
