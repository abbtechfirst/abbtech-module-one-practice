package HomeWork9.AbstractFactory;

abstract public class AbstractFactory {
    abstract Publication getCategory(String category);
}
