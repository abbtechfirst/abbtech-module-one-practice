package HomeWork9.AbstractFactory;

public class PhysicalVersion extends AbstractFactory {
    @Override
    Publication getCategory(String category) {
        if (category.equalsIgnoreCase("book")){
            return new Book();
        }
        else if (category.equalsIgnoreCase("journal")){
            return new Journal();
        }
        else if (category.equalsIgnoreCase("encyclopedia")){
            return new Encyclopedia();
        }
        return null;
    }
}
