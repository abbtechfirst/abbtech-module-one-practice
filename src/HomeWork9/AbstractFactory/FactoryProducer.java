package HomeWork9.AbstractFactory;

public class FactoryProducer {

    public static AbstractFactory getVersion(boolean physical){
        if (physical){
            return new PhysicalVersion();
        }
        else{
            return new DigitalVersion();
        }

    }
}
