package HomeWork9.AbstractFactory;

public interface Publication {
    void display();
}

class Book implements Publication{
    @Override
    public void display() {
        System.out.println("This is a book");
    }
}
class DigitalBook implements Publication{
    @Override
    public void display() {
        System.out.println("This is a pdf book");
    }
}

class Journal implements Publication{
    @Override
    public void display() {
        System.out.println("This is a journal");
    }
}
class DigitalJournal implements Publication{
    @Override
    public void display() {
        System.out.println("This is a digital journal");
    }
}
class Encyclopedia implements Publication{
    @Override
    public void display() {
        System.out.println("This is a encyclopedi");
    }
}
class DigitalEncyclopedia implements Publication{
    @Override
    public void display() {
        System.out.println("This is a digital encyclopedi");
    }
}