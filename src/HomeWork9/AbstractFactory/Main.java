package HomeWork9.AbstractFactory;


public class Main {
    public static void main(String[] args) {
        AbstractFactory version = FactoryProducer.getVersion(true);
        Publication publication1=version.getCategory("journal");
        publication1.display();
    }
}