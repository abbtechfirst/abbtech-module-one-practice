package HomeWork1;

import java.util.Scanner;

public class Exercise2 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        for (int i = 1; i <= number ; i++) {
            if(isPrime(i)<=2){
                System.out.println(i);
            }
        }
    }
    static int isPrime(int number){
        int count=1;
        for (int i = 2; i <= number ; i++) {
            if (number%i==0){
                count+=1;
            }
        }
        return count;
    }
}
