package HomeWork1;

import java.util.Scanner;

public class Exercise8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter first number ");
        int a= input.nextInt();
        System.out.println("Enter second number ");
        int b= input.nextInt();
        System.out.println("Enter operation ");
        String operation=input.next();
        switch (operation){
            case "+":
                System.out.println(a+b);
                break;
            case "*":
                System.out.println(a*b);
                break;
            case "-":
                System.out.println(a-b);
                break;
            case "/":
                System.out.println((double)a/b);
                break;
        }
    }
}
