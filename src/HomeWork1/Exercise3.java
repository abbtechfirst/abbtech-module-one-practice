package HomeWork1;

import java.util.Scanner;

public class Exercise3 {
    public static void main(String[] args){
        Scanner input =new Scanner(System.in);
        int number=input.nextInt();
        int last;
        int sum=0;
        while(number>0){
            last=number%10;
            sum=sum+last;
            number=number/10;
        }
        System.out.println(sum);
    }
}
